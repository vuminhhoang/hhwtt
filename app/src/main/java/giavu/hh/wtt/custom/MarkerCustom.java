package giavu.hh.wtt.custom;

import android.app.Activity;
import android.content.Context;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.model.Marker;

import giavu.hh.wtt.R;
import giavu.hh.wtt.networking.model.MarkerMap;

/**
 * @Author: Hoang Vu
 * @Date: 2018/06/22
 */

public class MarkerCustom implements GoogleMap.InfoWindowAdapter {

    private Context mContext;

    ImageView mImage;
    TextView mLicenseNo;
    TextView mDriverName;
    TextView mPhoneNo;
    TextView mDriverClass;

    public MarkerCustom(Context context){
        this.mContext = context;
    }
    @Override
    public View getInfoWindow(Marker marker) {
        return null;
    }

    @Override
    public View getInfoContents(Marker marker) {
        View view = ((Activity)mContext).getLayoutInflater()
                .inflate(R.layout.map_custom_infowindow, null);
        mImage = view.findViewById(R.id.pic);
        mLicenseNo = view.findViewById(R.id.license_driver);
        mDriverName = view.findViewById(R.id.name);
        mPhoneNo = view.findViewById(R.id.phone);
        mDriverClass = view.findViewById(R.id.rate);

        MarkerMap markerMap = (MarkerMap) marker.getTag();
        mImage.setImageDrawable(mContext.getResources().getDrawable(R.drawable.ic_driver_48));
        mLicenseNo.setText(markerMap.getLicense());
        mDriverName.setText(markerMap.getName());
        mPhoneNo.setText(markerMap.getPhoneNumber());
        StringBuilder classDriver = new StringBuilder();
        for(int i = 0 ; i < markerMap.getRating().intValue(); i++){
            classDriver.append("⭐︎");
        }
        mDriverClass.setText(classDriver);
        return view;
    }
}
