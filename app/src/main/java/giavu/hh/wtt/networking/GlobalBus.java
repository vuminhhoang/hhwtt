package giavu.hh.wtt.networking;

import org.greenrobot.eventbus.EventBus;

/**
 * @Author: Hoang Vu
 * @Date: 2018/06/22
 */

public class GlobalBus {
    private static EventBus eventBus;
    public static EventBus instanceEventBus(){
        if(eventBus == null){
            eventBus = EventBus.getDefault();
        }
        return eventBus;
    }
}
