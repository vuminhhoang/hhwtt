package giavu.hh.wtt.networking.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

/**
 * Created by hoangvm on 2018/06/21.
 */
public class ResVenues {
    private Venues[] venues;

    public Venues[] getVenues() {
        return venues;
    }

    public void setVenues(Venues[] venues) {
        this.venues = venues;
    }
}
