package giavu.hh.wtt.networking.model;

/**
 * Created by hoangvm on 2018/06/21.
 */
public class Point {
    private Double lat;
    private Double lon;

    public Double getLat() {
        if(lat == 0){
         return new Double(35.681382);
        }
        return lat;
    }

    public void setLat(Double lat) {
        this.lat = lat;
    }

    public Double getLon() {
        if(lon == 0){
            return new Double(139.7660839);
        }
        return lon;
    }

    public void setLon(Double lon) {
        this.lon = lon;
    }
}
