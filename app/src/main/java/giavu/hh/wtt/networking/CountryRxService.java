package giavu.hh.wtt.networking;

import com.google.gson.FieldNamingPolicy;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.jakewharton.retrofit2.adapter.rxjava2.RxJava2CallAdapterFactory;

import io.reactivex.schedulers.Schedulers;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

/**
 * Created by hoangvm on 2018/06/11.
 */
public class CountryRxService {
    // Define Base URL
    private static final String API = "http://services.groupkt.com/";


    // instance variable
    private static CountryRxService instance;

    public static CountryRxService getInstance(){
        if(instance == null){
            instance = new CountryRxService();
        }
        return instance;
    }
    public CountryIF getAPI(){
        final Gson gson = new GsonBuilder().setFieldNamingPolicy(FieldNamingPolicy.LOWER_CASE_WITH_UNDERSCORES).create();

        final Retrofit retrofit = new Retrofit.Builder().baseUrl(API)
                                                        .addCallAdapterFactory(RxJava2CallAdapterFactory.createWithScheduler(Schedulers.io()))
                                                        .addConverterFactory(GsonConverterFactory.create(gson))
                                                        .build();
        return retrofit.create(CountryIF.class);
    }

}
