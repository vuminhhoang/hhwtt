package giavu.hh.wtt.networking;

import android.util.Log;

import giavu.hh.wtt.contract.CountryContract;
import giavu.hh.wtt.model.CountryModel;
import giavu.hh.wtt.networking.model.ResData;
import io.reactivex.Observable;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.functions.Consumer;
import io.reactivex.functions.Function;
import io.reactivex.schedulers.Schedulers;

/**
 * Created by hoangvm on 2018/06/12.
 */
public class CountryCallBackImpl implements CountryContract.CountryCallBack {

    public final String TAG = CountryCallBackImpl.class.getSimpleName();

    @Override
    public void getCountryList(final OnFinishListener onFinishListener) {
        // Get list Data
        Observable<ResData> response = CountryRxService
                .getInstance()
                .getAPI()
                .getCountryList();

        // Add observe and subscribe
        response.subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(
                        new Consumer<ResData>() {
                            @Override
                            public void accept(ResData countries) throws Exception {
                                Log.d(TAG, "Retrieve data from server ...");
                                onFinishListener.onFinished(countries);
                            }
                        },
                        new Consumer<Throwable>() {
                            @Override
                            public void accept(Throwable throwable) throws Exception {
                                Log.d(TAG, "HTTP error : " + throwable.getMessage());
                                onFinishListener.onFailure(throwable);
                            }
                        }

                );
    }
}
