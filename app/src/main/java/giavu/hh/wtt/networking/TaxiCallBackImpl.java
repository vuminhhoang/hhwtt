package giavu.hh.wtt.networking;

import java.util.List;

import giavu.hh.wtt.contract.DriverContract;
import giavu.hh.wtt.networking.model.Point;
import giavu.hh.wtt.networking.model.Venues;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * @Author: Hoang Vu
 * @Date: 2018/06/22
 */

public class TaxiCallBackImpl implements DriverContract.TaxiCallback {

    @Override
    public void getDriverList(Point p, final onFinishListener listener) {
        Call<List<Venues>> res = DriverRxService
                .getInstance()
                .getAPI()
                .getListDriver(p.getLon(), p.getLat(), 100);
        res.enqueue(new Callback<List<Venues>>() {
            @Override
            public void onResponse(Call<List<Venues>> call, Response<List<Venues>> response) {
                listener.onFinished(response.body());
            }

            @Override
            public void onFailure(Call<List<Venues>> call, Throwable t) {
                listener.onFailure(t);
            }
        });
    }
}
