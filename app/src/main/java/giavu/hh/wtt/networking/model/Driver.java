package giavu.hh.wtt.networking.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

/**
 * @Author: Hoang Vu
 * @Date: 2018/06/20
 * "country": "jp",
"localized_country_name": "Japan",
"distance": 7.198555220386644,
"city": "Tokyo",
"lon": 139.77000427246094,
"ranking": 0,
"id": 1023444,
"member_count": 7706,
"lat": 35.66999816894531
 */

public class Driver {
    @SerializedName("zip")
    @Expose
    private String zip;

    @SerializedName("country")
    @Expose
    private String country;

    @SerializedName("localized_country_name")
    @Expose
    private String localized_country_name;

    @SerializedName("distance")
    @Expose
    private String distance;

    @SerializedName("city")
    @Expose
    private String city;

    @SerializedName("lon")
    @Expose
    private String lon;

    @SerializedName("ranking")
    @Expose
    private String ranking;

    @SerializedName("id")
    @Expose
    private String id;

    @SerializedName("member_count")
    @Expose
    private String member_count;

    @SerializedName("lat")
    @Expose
    private String lat;

    public String getZip() {
        return zip;
    }

    public void setZip(String zip) {
        this.zip = zip;
    }

    public String getCountry() {
        return country;
    }

    public void setCountry(String country) {
        this.country = country;
    }

    public String getLocalized_country_name() {
        return localized_country_name;
    }

    public void setLocalized_country_name(String localized_country_name) {
        this.localized_country_name = localized_country_name;
    }

    public String getDistance() {
        return distance;
    }

    public void setDistance(String distance) {
        this.distance = distance;
    }

    public String getCity() {
        return city;
    }

    public void setCity(String city) {
        this.city = city;
    }

    public String getLon() {
        return lon;
    }

    public void setLon(String lon) {
        this.lon = lon;
    }

    public String getRanking() {
        return ranking;
    }

    public void setRanking(String ranking) {
        this.ranking = ranking;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getMember_count() {
        return member_count;
    }

    public void setMember_count(String member_count) {
        this.member_count = member_count;
    }

    public String getLat() {
        return lat;
    }

    public void setLat(String lat) {
        this.lat = lat;
    }
}
