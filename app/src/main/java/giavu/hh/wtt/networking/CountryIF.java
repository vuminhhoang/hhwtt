package giavu.hh.wtt.networking;


import giavu.hh.wtt.networking.model.ResData;
import retrofit2.http.GET;

/**
 * Created by hoangvm on 2018/06/11.
 */
public interface CountryIF {
    @GET("country/get/all")
    io.reactivex.Observable<ResData> getCountryList();
}
