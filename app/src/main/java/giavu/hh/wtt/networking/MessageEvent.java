package giavu.hh.wtt.networking;

/**
 * @Author: Hoang Vu
 * @Date: 2018/06/22
 */

public class MessageEvent {
    private String message;

    public MessageEvent(String message) {
        this.message = message;
    }

    public String getMessage() {
        return message;
    }
}
