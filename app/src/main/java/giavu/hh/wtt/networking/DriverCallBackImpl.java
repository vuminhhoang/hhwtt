package giavu.hh.wtt.networking;

import java.util.List;

import giavu.hh.wtt.contract.HoiMapsContract;
import giavu.hh.wtt.networking.model.Point;
import giavu.hh.wtt.networking.model.ResultDriverData;
import giavu.hh.wtt.networking.model.Venues;
import io.reactivex.Observable;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.functions.Consumer;
import io.reactivex.schedulers.Schedulers;

/**
 * @Author: Hoang Vu
 * @Date: 2018/06/20
 */

public class DriverCallBackImpl implements HoiMapsContract.DriverCallback{

    @Override
    public void getDriverNearby(final OnFinishListener onFinishListener) {
        Observable<ResultDriverData> response = DriverRxService
                .getInstance()
                .getAPI()
                .getDriverNearby(139.7660839, 35.681382,50 );
        response.subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(new Consumer<ResultDriverData>() {
                               @Override
                               public void accept(ResultDriverData resultDriverData) throws Exception {
                                    onFinishListener.onFinished(resultDriverData);
                               }
                           },
                            new Consumer<Throwable>() {
                                @Override
                                public void accept(Throwable throwable) throws Exception {
                                    onFinishListener.onFailure(throwable);
                                }
                });
    }


    @Override
    public void getVenuesList(Point p, final OnFinishListenerVenues onFinishListenerVenues) {
        Observable<List<Venues>> response = DriverRxService
                .getInstance()
                .getAPI()
                .getVenuesRecommend(p.getLon(), p.getLat(),50 );
        response.subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(new Consumer<List<Venues>>() {
                               @Override
                               public void accept(List<Venues> venues) throws Exception {
                                    onFinishListenerVenues.onFinished(venues);
                               }
                           },
                        new Consumer<Throwable>() {
                            @Override
                            public void accept(Throwable throwable) throws Exception {
                                    onFinishListenerVenues.onFailure(throwable);
                            }
                        }
                );
    }
}
