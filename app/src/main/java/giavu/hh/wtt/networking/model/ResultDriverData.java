package giavu.hh.wtt.networking.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

/**
 * @Author: Hoang Vu
 * @Date: 2018/06/20
 */

public class ResultDriverData {
    @SerializedName("results")
    @Expose
    private List<Driver> drives = null;

    public List<Driver> getDrives() {
        return drives;
    }

    public void setDrives(List<Driver> drives) {
        this.drives = drives;
    }
}
