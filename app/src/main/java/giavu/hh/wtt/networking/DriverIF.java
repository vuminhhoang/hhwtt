package giavu.hh.wtt.networking;

import java.util.List;

import giavu.hh.wtt.networking.model.ResultDriverData;
import giavu.hh.wtt.networking.model.Venues;
import giavu.hh.wtt.utils.Constants;
import retrofit2.Call;
import retrofit2.http.GET;
import retrofit2.http.Query;

/**
 * @Author: Hoang Vu
 * @Date: 2018/06/20
 */

public interface DriverIF {

    @GET("2/cities?key="+ Constants.API_KEY+"&sign=true&photo-host=public&page=10")
    io.reactivex.Observable<ResultDriverData> getDriverNearby(
            @Query("lon") double lon,
            @Query("lat") double lat,
            @Query("radius") double radius
            );

    @GET("/recommended/venues?key="+ Constants.API_KEY+"&sign=true&photo-host=public&page=10")
    io.reactivex.Observable<List<Venues>> getVenuesRecommend(
            @Query("lon") double lon,
            @Query("lat") double lat,
            @Query("radius") double radius
    );

    @GET("/recommended/venues?key="+ Constants.API_KEY+"&sign=true&photo-host=public&page=20")
    Call<List<Venues>> getListDriver(
            @Query("lon") double lon,
            @Query("lat") double lat,
            @Query("radius") double radius
    );

}
