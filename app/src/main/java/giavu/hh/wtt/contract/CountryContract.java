package giavu.hh.wtt.contract;

import java.util.List;

import giavu.hh.wtt.networking.model.Result;
import giavu.hh.wtt.networking.model.ResData;

/**
 * Created by hoangvm on 2018/06/11.
 */
public interface CountryContract {
    interface View{
        void initView();
        void showProgress();
        void hideProgress();
        void setDataToRecycleView(List<Result> list);
        void onResponseFailure(Throwable throwable);
    }

    interface Model{

    }

    interface Presenter{
        void onDestroy();
        void onRefreshButtonOnclick();
        void requestDataFromServer();

    }

    interface CountryCallBack{
        interface OnFinishListener{
            void onFinished(ResData resData);
            void onFailure(Throwable t);
        }

        void getCountryList(OnFinishListener onFinishListener);
    }
}
