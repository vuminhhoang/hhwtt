package giavu.hh.wtt.contract;

import java.util.List;

import giavu.hh.wtt.networking.model.MarkerMap;
import giavu.hh.wtt.networking.model.Point;
import giavu.hh.wtt.networking.model.Venues;

/**
 * @Author: Hoang Vu
 * @Date: 2018/06/22
 */

public interface DriverContract {
    interface View{
        void initView();
        void showProgress();
        void hideProgress();
        void fetchDataError(Throwable throwable);
        void fillRecycleView(List<MarkerMap> list);
    }

    interface Model{

    }

    interface Presenter{
        void onDestroy();
        void fetchDataTaxi(Point p);
    }

    interface TaxiCallback{
        interface onFinishListener{
            void onFinished(List<Venues> data);
            void onFailure(Throwable throwable);
        }
        void getDriverList(Point p , onFinishListener listener);
    }
}
