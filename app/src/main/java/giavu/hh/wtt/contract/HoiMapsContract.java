package giavu.hh.wtt.contract;

import java.util.List;

import giavu.hh.wtt.networking.model.MarkerMap;
import giavu.hh.wtt.networking.model.Point;
import giavu.hh.wtt.networking.model.ResultDriverData;
import giavu.hh.wtt.networking.model.Venues;

/**
 * @Author: Hoang Vu
 * @Date: 2018/06/20
 */

public interface HoiMapsContract {
    interface View{
        void initView();
        void showProgress();
        void hideProgress();
        void onResponseFailure(Throwable throwable);
        void showMarkerNearCP(List<MarkerMap> v);
    }

    interface Model{

    }

    interface Presenter{
        void clickMenu(android.view.View view, Point p);
        void onDestroy();
        void findDriver();
        void findVenues(Point p);
        void setCurrentPos(Point pos);
        void phoneCall(String number);
    }

    interface DriverCallback{
        interface OnFinishListener{
            void onFinished(ResultDriverData resData);
            void onFailure(Throwable t);
        }

        interface OnFinishListenerVenues{
            void onFinished(List<Venues> resData);
            void onFailure(Throwable t);
        }

        void getDriverNearby(OnFinishListener onFinishListener);
        void getVenuesList(Point p , OnFinishListenerVenues onFinishListener);
    }
}
