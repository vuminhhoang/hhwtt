package giavu.hh.wtt.contract;

/**
 * @Author: Hoang Vu
 * @Date: 6/8/2018
 */

public interface MainContract {
    // View interface
    interface View{
        void initView();
        void navigateToMap();
    }

    // Model interface
    interface Model{

    }

    // Presenter interface
    interface Presenter{
        void onClickHOIMAP(android.view.View view);
    }
}
