package giavu.hh.wtt.adapter;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.List;

import giavu.hh.wtt.R;
import giavu.hh.wtt.networking.model.Result;

/**
 * Created by hoangvm on 2018/06/11.
 */
public class CountryAdapter extends RecyclerView.Adapter<CountryAdapter.MyViewHolder>{

    private List<Result> data;
    private Context context;

    public CountryAdapter(Context context, List<Result> data){
        this.data = data;
        this.context = context;
    }
    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.row_layout, parent,false);
        return new MyViewHolder(view);
    }

    @Override
    public void onBindViewHolder(MyViewHolder holder, int position) {
        holder.tvName.setText(data.get(position).getName());
        holder.alphaCode2.setText(data.get(position).getAlpha2Code());
        holder.alphaCode3.setText(data.get(position).getAlpha3Code());
    }

    @Override
    public int getItemCount() {
        return data.size();
    }

    public class MyViewHolder extends RecyclerView.ViewHolder{
        TextView tvName;
        TextView alphaCode2;
        TextView alphaCode3;
        public MyViewHolder(View itemView) {
            super(itemView);
            tvName = itemView.findViewById(R.id.name);
            alphaCode2 = itemView.findViewById(R.id.alpha2);
            alphaCode3 = itemView.findViewById(R.id.alpha3);
        }

    }

}
