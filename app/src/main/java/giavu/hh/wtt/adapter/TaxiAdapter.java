package giavu.hh.wtt.adapter;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;

import java.util.List;

import giavu.hh.wtt.R;
import giavu.hh.wtt.networking.model.MarkerMap;
import giavu.hh.wtt.utils.Utils;
import giavu.hh.wtt.view.DriverActivity;
import giavu.hh.wtt.view.HoiMapsActivity;

/**
 * @Author: Hoang Vu
 * @Date: 2018/06/22
 */

public class TaxiAdapter extends RecyclerView.Adapter<TaxiAdapter.MyViewHolder>{

    private Context mContext;
    private List<MarkerMap> list;

    public TaxiAdapter(Context context, List<MarkerMap> markerMaps){
        this.mContext = context;
        this.list = markerMaps;
    }
    @NonNull
    @Override
    public MyViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.driver_item, parent, false);
        return new MyViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull MyViewHolder holder, final int position) {
        holder.name.setText(list.get(position).getName());
        holder.company.setText(Utils.getRandomCompanyName(mContext));
        StringBuilder s = new StringBuilder();
        for(int i = 0; i < list.get(position).getRating().intValue(); i ++){
            s.append("⭐️");
        }
        holder.classMember.setText(s);
        Glide.with(mContext)
                .load(R.drawable.ic_driver_48)
                .asBitmap()
                .fitCenter()
                .placeholder(R.drawable.progress_image)
                .into(holder.avatar);
        holder.itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(mContext, HoiMapsActivity.class);
                ((DriverActivity)mContext).setResult(Activity.RESULT_OK);
                ((DriverActivity) mContext).finish();
            }
        });

    }

    @Override
    public int getItemCount() {
        return list.size();
    }

    public class MyViewHolder extends RecyclerView.ViewHolder{
        ImageView avatar;
        TextView name;
        TextView license;
        TextView phone;
        TextView classMember;
        TextView company;
        public MyViewHolder(View itemView) {
            super(itemView);
            avatar = itemView.findViewById(R.id.driver_avatar);
            name = itemView.findViewById(R.id.driver_name);
            license = itemView.findViewById(R.id.license_driver);
            classMember = itemView.findViewById(R.id.driver_class);
            company = itemView.findViewById(R.id.company);

        }

    }
}
