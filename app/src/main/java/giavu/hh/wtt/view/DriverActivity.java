package giavu.hh.wtt.view;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.DividerItemDecoration;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.Gravity;
import android.view.View;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;

import java.util.List;

import butterknife.ButterKnife;
import giavu.hh.wtt.R;
import giavu.hh.wtt.adapter.TaxiAdapter;
import giavu.hh.wtt.contract.DriverContract;
import giavu.hh.wtt.networking.TaxiCallBackImpl;
import giavu.hh.wtt.networking.model.MarkerMap;
import giavu.hh.wtt.networking.model.Point;
import giavu.hh.wtt.presenter.DriverPresenter;
import giavu.hh.wtt.utils.Constants;

public class DriverActivity extends AppCompatActivity
        implements DriverContract.View{

    private ProgressBar progressBar;
    private RecyclerView recyclerView;
    private DriverPresenter mPresenter;
    private TaxiAdapter taxiAdapter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_driver);
        mPresenter = new DriverPresenter(this, new TaxiCallBackImpl(), DriverActivity.this);
        initProgressBar();
        initRecycleView();
        Point p = new Point();
        if(getIntent().hasExtra(Constants.LOCATION_LONGITUDE_DATA_EXTRA)){
            p.setLon(getIntent().getDoubleExtra(Constants.LOCATION_LONGITUDE_DATA_EXTRA,139.7660839));
        }
        if(getIntent().hasExtra(Constants.LOCATION_LATITUDE_DATA_EXTRA)){
            p.setLat(getIntent().getDoubleExtra(Constants.LOCATION_LATITUDE_DATA_EXTRA, 35.681382));
        }
        mPresenter.fetchDataTaxi(p);
    }

    @Override
    public void initView() {
        ButterKnife.bind(this);
    }

    @Override
    public void showProgress() {
        progressBar.setVisibility(View.VISIBLE);
    }

    @Override
    public void hideProgress() {
        progressBar.setVisibility(View.INVISIBLE);
    }

    @Override
    public void fetchDataError(Throwable throwable) {

    }

    @Override
    public void fillRecycleView(List<MarkerMap> list) {
        taxiAdapter = new TaxiAdapter(this, list);
        recyclerView.addItemDecoration(new DividerItemDecoration(this, LinearLayoutManager.VERTICAL));
        recyclerView.setAdapter(taxiAdapter);
        recyclerView.setLayoutManager(new LinearLayoutManager(this));
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        mPresenter.onDestroy();
    }

    private void initRecycleView(){
        recyclerView = findViewById(R.id.driverList);
    }

    private void initProgressBar(){

        progressBar = new ProgressBar(this, null, android.R.attr.progressBarStyleLarge);
        progressBar.setIndeterminate(true);

        RelativeLayout relativeLayout = new RelativeLayout(this);
        relativeLayout.setGravity(Gravity.CENTER);
        relativeLayout.addView(progressBar);

        RelativeLayout.LayoutParams params = new
                RelativeLayout.LayoutParams(RelativeLayout.LayoutParams.MATCH_PARENT, RelativeLayout.LayoutParams.MATCH_PARENT);
        progressBar.setVisibility(View.INVISIBLE);

        this.addContentView(relativeLayout, params);
    }
}
