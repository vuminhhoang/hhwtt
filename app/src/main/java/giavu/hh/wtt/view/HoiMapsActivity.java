package giavu.hh.wtt.view;

import android.Manifest;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.location.Address;
import android.location.Criteria;
import android.location.Geocoder;
import android.location.Location;
import android.location.LocationManager;
import android.os.Build;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.ActivityCompat;
import android.support.v4.app.FragmentActivity;
import android.support.v4.content.ContextCompat;
import android.util.Log;
import android.view.View;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.location.LocationListener;
import com.google.android.gms.location.LocationRequest;
import com.google.android.gms.location.LocationServices;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.CameraPosition;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;
import com.google.android.gms.maps.model.PointOfInterest;
import com.google.maps.android.SphericalUtil;
import com.tbruyelle.rxpermissions2.RxPermissions;

import org.greenrobot.eventbus.Subscribe;
import org.greenrobot.eventbus.ThreadMode;

import java.io.IOException;
import java.util.List;
import java.util.Locale;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import giavu.hh.wtt.R;
import giavu.hh.wtt.contract.HoiMapsContract;
import giavu.hh.wtt.custom.MarkerCustom;
import giavu.hh.wtt.networking.DriverCallBackImpl;
import giavu.hh.wtt.networking.GeocodeAddressIntentService;
import giavu.hh.wtt.networking.GlobalBus;
import giavu.hh.wtt.networking.MessageEvent;
import giavu.hh.wtt.networking.model.MarkerMap;
import giavu.hh.wtt.networking.model.Point;
import giavu.hh.wtt.presenter.HoiMapsPresenter;
import giavu.hh.wtt.utils.Constants;
import giavu.hh.wtt.utils.Utils;

public class HoiMapsActivity extends FragmentActivity
        implements HoiMapsContract.View
        ,GoogleMap.OnPoiClickListener
        ,GoogleMap.OnMapClickListener
        ,GoogleMap.OnMapLoadedCallback
        ,OnMapReadyCallback
        ,GoogleMap.OnCameraMoveStartedListener
        ,GoogleMap.OnCameraMoveListener
        ,GoogleMap.OnCameraMoveCanceledListener
        ,GoogleMap.OnCameraIdleListener
        , GoogleApiClient.ConnectionCallbacks
        , GoogleApiClient.OnConnectionFailedListener
        , LocationListener
        , GoogleMap.OnMarkerClickListener{

    private final String TAG = HoiMapsActivity.class.getSimpleName();
    private GoogleMap mMap;
    private GoogleApiClient mGoogleApiClient;
    private Location mLastLocation;
    private Marker mCurrLocationMarker;
    private LocationRequest mLocationRequest;
    //private SearchView mSearchView;
    private ImageView mPickupMe;
    private TextView mAddress;
    private LinearLayout llAddress;
    public static final int MY_PERMISSIONS_REQUEST_LOCATION = 99;

    private CameraPosition cameraPosition;

    private ProgressBar progressBar;
    private HoiMapsPresenter mPresenter;

    private boolean isMarkerClick;
    private Marker mMarker;
    @BindView(R.id.menu)
    ImageButton mMenu;

    RxPermissions rxPermissions;

    @OnClick(R.id.menu)
    public void onClickMenu(View view){
        Point p = new Point();
        p.setLat(cameraPosition.target.latitude);
        p.setLon(cameraPosition.target.longitude);
        mPresenter.clickMenu(view, p);
    }

    @Override
    protected void onStart() {
        super.onStart();

        GlobalBus.instanceEventBus().register(this);
    }

    @Override
    protected void onStop() {
        super.onStop();

        GlobalBus.instanceEventBus().unregister(this);
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_hoi_maps);
        ButterKnife.bind(this);
        rxPermissions = new RxPermissions(this);
        if (android.os.Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            checkLocationPermission();
            //permissionRxJavaCheck();
        }
        mPresenter = new HoiMapsPresenter(this,new DriverCallBackImpl(),HoiMapsActivity.this);
        //mSearchView = findViewById(R.id.searchView);
        mPickupMe = findViewById(R.id.cross_icon);

        mAddress = findViewById(R.id.address);
        llAddress = findViewById(R.id.ll_addBar);
        // Height of status bar
        int heightOfStatusbar = Utils.getStatusBarHeight(HoiMapsActivity.this);
        // Margin of bottom bar
        // 40 is size of pickup icon
        int marginBottomAdd = Utils.dpToPx(40) + heightOfStatusbar;
        RelativeLayout.LayoutParams params = new RelativeLayout.LayoutParams(Utils.dpToPx(40), Utils.dpToPx(40));
        params.setMargins(mPickupMe.getLeft(), mPickupMe.getTop(), mPickupMe.getRight(), mPickupMe.getBottom() + marginBottomAdd / 2);
        mPickupMe.setLayoutParams(params);


        // Obtain the SupportMapFragment and get notified when the map is ready to be used.
        SupportMapFragment mapFragment = (SupportMapFragment) getSupportFragmentManager()
                .findFragmentById(R.id.map);
        mapFragment.getMapAsync(this);

        View mapView = mapFragment.getView();
        // Get map views
        if (mapView != null &&
                mapView.findViewById(Integer.parseInt("1")) != null) {
            // Get the button view
            View location_button = ((View) mapView.findViewById(Integer.parseInt("1"))
                    .getParent()).findViewById(Integer.parseInt("2"));
            //View location_button =mapView.findViewWithTag("GoogleMapMyLocationButton");
            //View zoom_in_button = mapView.findViewWithTag("GoogleMapZoomInButton");
            //View zoom_layout = (View) zoom_in_button.getParent();

            // adjust location button layout params above the zoom layout
            RelativeLayout.LayoutParams location_layout = (RelativeLayout.LayoutParams) location_button.getLayoutParams();
            location_layout.addRule(RelativeLayout.ALIGN_PARENT_TOP, 0);
            location_layout.addRule(RelativeLayout.ALIGN_PARENT_BOTTOM, RelativeLayout.TRUE);
            location_layout.setMargins(0, 0, 50, 200);
            //location_layout.addRule(RelativeLayout.ABOVE, zoom_layout.getId());
        }
    }


    /**
     * Manipulates the map once available.
     * This callback is triggered when the map is ready to be used.
     * This is where we can add markers or lines, add listeners or move the camera. In this case,
     * we just add a marker near Sydney, Australia.
     * If Google Play services is not installed on the device, the user will be prompted to install
     * it inside the SupportMapFragment. This method will only be triggered once the user has
     * installed Google Play services and returned to the app.
     */
    @Override
    public void onMapReady(GoogleMap googleMap) {
        Log.d(TAG, "Map ready to use!!!");
        mMap = googleMap;
        cameraPosition = mMap.getCameraPosition();
        mMap.setOnCameraIdleListener(this);
        mMap.setOnCameraMoveStartedListener(this);
        mMap.setOnCameraMoveListener(this);
        mMap.setOnCameraMoveCanceledListener(this);
        mMap.setOnMapLoadedCallback(this);
        mMap.setOnMarkerClickListener(this);

        mMap.setMapType(GoogleMap.MAP_TYPE_NORMAL);
        //mMap.getUiSettings().setZoomControlsEnabled(true);
        mMap.getUiSettings().setZoomGesturesEnabled(true);
        mMap.getUiSettings().setCompassEnabled(true);

        initGooglePlayService();

    }

    private void initGooglePlayService(){
        //Initialize Google Play Services
        if (android.os.Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            if (ContextCompat.checkSelfPermission(this,
                    Manifest.permission.ACCESS_FINE_LOCATION)
                    == PackageManager.PERMISSION_GRANTED) {
                buildGoogleApiClient();
                mMap.setMyLocationEnabled(true);
            }
        } else {
            buildGoogleApiClient();
            mMap.setMyLocationEnabled(true);
        }

    }


    @Override
    public void onConnected(@Nullable Bundle bundle) {
        mLocationRequest = new LocationRequest();
        mLocationRequest.setInterval(1000);
        mLocationRequest.setFastestInterval(1000);
        mLocationRequest.setPriority(LocationRequest.PRIORITY_BALANCED_POWER_ACCURACY);
        if (ContextCompat.checkSelfPermission(this,
                Manifest.permission.ACCESS_FINE_LOCATION)
                == PackageManager.PERMISSION_GRANTED) {
            LocationServices.FusedLocationApi.requestLocationUpdates(mGoogleApiClient,mLocationRequest,this);
        }
    }

    @Override
    public void onConnectionSuspended(int i) {

    }

    @Override
    public void onConnectionFailed(@NonNull ConnectionResult connectionResult) {

    }

    @Override
    public void onLocationChanged(Location location) {
        mLastLocation = location;
        if (mCurrLocationMarker != null) {
            mCurrLocationMarker.remove();
        }
        //Showing Current Location MarkerMap on Map
        LatLng latLng = new LatLng(location.getLatitude(), location.getLongitude());
        MarkerOptions markerOptions = new MarkerOptions();
        markerOptions.position(latLng);
        LocationManager locationManager = (LocationManager)
                getSystemService(Context.LOCATION_SERVICE);
        String provider = locationManager.getBestProvider(new Criteria(), true);
        if (ActivityCompat.checkSelfPermission(this,
                Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED &&
                ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_COARSE_LOCATION)
                        != PackageManager.PERMISSION_GRANTED) {
            return;
        }
        Location locations = locationManager.getLastKnownLocation(provider);
        List<String> providerList = locationManager.getAllProviders();
        if (null != locations && null != providerList && providerList.size() > 0) {
            double longitude = locations.getLongitude();
            double latitude = locations.getLatitude();
            Geocoder geocoder = new Geocoder(getApplicationContext(),
                    Locale.getDefault());
            try {
                List<Address> listAddresses = geocoder.getFromLocation(latitude,
                        longitude, 1);
                if (null != listAddresses && listAddresses.size() > 0) {
                    String state = listAddresses.get(0).getAdminArea();
                    String country = listAddresses.get(0).getCountryName();
                    String subLocality = listAddresses.get(0).getSubLocality();
                    markerOptions.title("" + latLng + "," + subLocality + "," + state
                            + "," + country);
                }
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
        //markerOptions.icon(BitmapDescriptorFactory.defaultMarker(BitmapDescriptorFactory.HUE_BLUE));
        //mCurrLocationMarker = mMap.addMarker(markerOptions);
        mMap.moveCamera(CameraUpdateFactory.newLatLng(latLng));
        mMap.animateCamera(CameraUpdateFactory.zoomTo(16.5F));
        if (mGoogleApiClient != null) {
            LocationServices.FusedLocationApi.removeLocationUpdates(mGoogleApiClient,
                    this);
        }
    }


    @Override
    public void onRequestPermissionsResult(int requestCode,
                                           String permissions[], int[] grantResults) {
        switch (requestCode) {
            case MY_PERMISSIONS_REQUEST_LOCATION: {
                if (grantResults.length > 0
                        && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                    if (ContextCompat.checkSelfPermission(this,
                            Manifest.permission.ACCESS_FINE_LOCATION)
                            == PackageManager.PERMISSION_GRANTED) {
                        if (mGoogleApiClient == null) {
                            buildGoogleApiClient();
                        }
                        mMap.setMyLocationEnabled(true);
                    }
                } else {
                    Toast.makeText(this, "permission denied",
                            Toast.LENGTH_LONG).show();
                }
                return;
            }
        }
    }

    public boolean checkLocationPermission() {
        if (ContextCompat.checkSelfPermission(this,
                Manifest.permission.ACCESS_FINE_LOCATION)
                != PackageManager.PERMISSION_GRANTED) {

            if (ActivityCompat.shouldShowRequestPermissionRationale(this,
                    Manifest.permission.ACCESS_FINE_LOCATION)) {
                ActivityCompat.requestPermissions(this,
                        new String[]{Manifest.permission.ACCESS_FINE_LOCATION},
                        MY_PERMISSIONS_REQUEST_LOCATION);
            } else {
                ActivityCompat.requestPermissions(this,
                        new String[]{Manifest.permission.ACCESS_FINE_LOCATION},
                        MY_PERMISSIONS_REQUEST_LOCATION);
            }
            return false;
        } else {
            return true;
        }
    }

    protected synchronized void buildGoogleApiClient() {
        mGoogleApiClient = new GoogleApiClient.Builder(this)
                .addConnectionCallbacks(this)
                .addOnConnectionFailedListener(this)
                .addApi(LocationServices.API)
                .build();
        mGoogleApiClient.connect();
    }


    @Override
    public void onCameraMoveCanceled() {
        Log.d(TAG,"Camera movement canceled");
    }

    @Override
    public void onCameraMove() {
        Log.d(TAG,"The camera is moving");
        //mSearchView.setVisibility(View.INVISIBLE);
        llAddress.setVisibility(View.INVISIBLE);
    }

    @Override
    public void onCameraMoveStarted(int reason) {
        if (reason == GoogleMap.OnCameraMoveStartedListener.REASON_GESTURE) {
            Log.d(TAG,"The user gestured on the map");
        } else if (reason == GoogleMap.OnCameraMoveStartedListener
                .REASON_API_ANIMATION) {
            Log.d(TAG,"The user tapped something on the map");
        } else if (reason == GoogleMap.OnCameraMoveStartedListener
                .REASON_DEVELOPER_ANIMATION) {
            Log.d(TAG,"The app moved the camera");
        }
    }

    @Override
    public void onCameraIdle() {
        Log.d(TAG, "Zoom value is : " + String.valueOf(mMap.getCameraPosition().zoom));
        //mPresenter.findDriver();
        if(isMarkerClick){
            // TODO
            isMarkerClick = false;
            return;
        }
        mPickupMe.setVisibility(View.VISIBLE);
        //mSearchView.setVisibility(View.VISIBLE);
        llAddress.setVisibility(View.VISIBLE);

        Log.d(TAG,"The camera has stopped moving");

        Point p = new Point();
        p.setLat(mMap.getCameraPosition().target.latitude);
        p.setLon(mMap.getCameraPosition().target.longitude);
        Log.d(TAG, "Lat center is : " + String.valueOf(p.getLat()));
        Log.d(TAG, "Lon center is : " + String.valueOf(p.getLon()));
        mPresenter.setCurrentPos(p);
        CameraPosition currentCmr = mMap.getCameraPosition();
        if(!cameraPosition.target.equals(currentCmr.target)){
            if (SphericalUtil.computeDistanceBetween(cameraPosition.target, currentCmr.target) > 100){
                mPresenter.findVenues(p);
                fetchLocationAddress(currentCmr.target);
                cameraPosition = currentCmr;
            }
        }
        //TODO
        // Do something when get new location
    }

    @Override
    public void initView() {

    }

    @Override
    public void showProgress() {

    }

    @Override
    public void hideProgress() {

    }

    @Override
    public void onResponseFailure(Throwable throwable) {

    }

    @Override
    public void showMarkerNearCP(List<MarkerMap> markes) {
        Log.d(TAG, "showMarker");
        mMap.clear();
        // adding marker
        Bitmap markerBitmap;
        for(MarkerMap marker : markes){
            if(marker.getRating() > 3){
                markerBitmap = BitmapFactory.decodeResource(getResources(), R.drawable.ic_motorcycle_48);
            }else{
                markerBitmap = BitmapFactory.decodeResource(getResources(), R.drawable.ic_motorcycle_2_48);
            }

            markerBitmap = Utils.scaleBitmap(markerBitmap, 80, 80);
            MarkerOptions m = marker.getMarkerOptions();
            m.icon(BitmapDescriptorFactory.fromBitmap(markerBitmap));
            Marker markerAdd = mMap.addMarker(m);
            MarkerCustom markerCustom = new MarkerCustom(HoiMapsActivity.this);
            markerAdd.setTag(marker);
            mMap.setInfoWindowAdapter(markerCustom);
            mMap.setOnInfoWindowClickListener(new GoogleMap.OnInfoWindowClickListener() {
                @Override
                public void onInfoWindowClick(Marker marker) {
                    Log.d(TAG, "Woooo! Detected");
                    mPresenter.phoneCall(((MarkerMap)marker.getTag()).getPhoneNumber());
                }
            });

        }

    }

    @Override
    public boolean onMarkerClick(Marker marker) {
        Log.d(TAG,"onMarkerClick");
        if (mMarker != null && !mMarker.getPosition().equals(marker.getPosition())){
            //mMarker.remove();
            mMarker = marker;
        }
        if (mMarker == null){
            mMarker = marker;
        }
        mMarker.showInfoWindow();
        mPickupMe.setVisibility(View.INVISIBLE);
        isMarkerClick = true;
        return false;
    }

    @Override
    public void onMapLoaded() {
        Log.d(TAG,"onMapLoaded");
    }

    @Override
    public void onMapClick(LatLng latLng) {
        Log.d(TAG,"onMapClick");
    }

    @Override
    public void onPoiClick(PointOfInterest pointOfInterest) {

    }

    private void fetchLocationAddress(LatLng latLng){
        Intent intent = new Intent(this, GeocodeAddressIntentService.class);
        intent.putExtra(Constants.LOCATION_LATITUDE_DATA_EXTRA, latLng.latitude);
        intent.putExtra(Constants.LOCATION_LONGITUDE_DATA_EXTRA, latLng.longitude);
        startService(intent);
    }

    @Subscribe(threadMode = ThreadMode.MAIN)
    public void onMessageEvent(MessageEvent event) {
        Log.d(TAG, "Received !!!" + event.getMessage());
        mAddress.setText(event.getMessage());
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if(requestCode == Constants.MAP_CODE && resultCode == RESULT_OK){
            Log.d(TAG,"Comeback !");
        }
    }
}
