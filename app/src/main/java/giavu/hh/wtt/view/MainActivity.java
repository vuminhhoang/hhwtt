package giavu.hh.wtt.view;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import butterknife.ButterKnife;
import butterknife.OnClick;
import giavu.hh.wtt.R;
import giavu.hh.wtt.contract.MainContract;
import giavu.hh.wtt.presenter.MainPresenter;

public class MainActivity extends AppCompatActivity implements MainContract.View{

    private MainContract.Presenter mPresenter;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        mPresenter = new MainPresenter(this);

    }
/*
    @OnClick(R.id.rxJavaRetrofit)
    public void gotoRx(View view){
        mPresenter.onClickRxJava(view);
    }

    @OnClick(R.id.lifecycle)
    public void onClickLifecycle(View view){
        mPresenter.onClickLifecycle(view);
    }
    @OnClick(R.id.countClick)
    public void onClickCount(View view){
        mPresenter.onCount(view);
    }*/

    @OnClick(R.id.map)
    public void onClickMap(View view){
        mPresenter.onClickHOIMAP(view);
    }

    @Override
    public void initView() {
        ButterKnife.bind(this);
    }

    @Override
    public void navigateToMap() {
        Intent i = new Intent(this, HoiMapsActivity.class);
        startActivity(i);
    }
}
