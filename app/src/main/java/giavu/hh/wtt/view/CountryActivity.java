package giavu.hh.wtt.view;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.Gravity;
import android.view.View;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.TableLayout;

import java.util.ArrayList;
import java.util.List;

import butterknife.ButterKnife;
import giavu.hh.wtt.R;
import giavu.hh.wtt.adapter.CountryAdapter;
import giavu.hh.wtt.contract.CountryContract;
import giavu.hh.wtt.networking.CountryCallBackImpl;
import giavu.hh.wtt.networking.model.Result;
import giavu.hh.wtt.presenter.CountryPresenter;

public class CountryActivity extends AppCompatActivity implements CountryContract.View{

    private ProgressBar progressBar;
    private RecyclerView recyclerView;
    private CountryPresenter mPresenter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_country);
        mPresenter = new CountryPresenter(this, new CountryCallBackImpl());
        initProgressBar();
        initRecycleView();
        mPresenter.requestDataFromServer();
    }

    private void initRecycleView(){
        recyclerView = findViewById(R.id.recycleView);
    }
    private void initProgressBar(){
        progressBar = new ProgressBar(this, null, android.R.attr.progressBarStyleLarge);
        progressBar.setIndeterminate(true);

        RelativeLayout relativeLayout = new RelativeLayout(this);
        relativeLayout.setGravity(Gravity.CENTER);
        relativeLayout.addView(progressBar);

        RelativeLayout.LayoutParams params = new
                RelativeLayout.LayoutParams(RelativeLayout.LayoutParams.MATCH_PARENT, RelativeLayout.LayoutParams.MATCH_PARENT);
        progressBar.setVisibility(View.INVISIBLE);

        this.addContentView(relativeLayout, params);
    }
    @Override
    public void initView() {
        ButterKnife.bind(this);
    }

    @Override
    public void showProgress() {
        progressBar.setVisibility(View.VISIBLE);
    }

    @Override
    public void hideProgress() {
        progressBar.setVisibility(View.INVISIBLE);
    }

    @Override
    public void setDataToRecycleView(List<Result> list) {
        CountryAdapter adapter = new CountryAdapter(CountryActivity.this, list);
        recyclerView.setAdapter(adapter);
        recyclerView.setLayoutManager(new LinearLayoutManager(this));
    }

    @Override
    public void onResponseFailure(Throwable throwable) {

    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        mPresenter.onDestroy();
    }
}
