package giavu.hh.wtt.view;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;

import giavu.hh.wtt.R;

public class LifecycleAwareActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_chat);

        getLifecycle().addObserver(new MyObserver());
    }
}
