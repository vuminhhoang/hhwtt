package giavu.hh.wtt.presenter;

import android.content.Context;
import android.util.Log;

import java.util.ArrayList;
import java.util.List;

import giavu.hh.wtt.contract.DriverContract;
import giavu.hh.wtt.model.DriverModel;
import giavu.hh.wtt.networking.model.MarkerMap;
import giavu.hh.wtt.networking.model.Point;
import giavu.hh.wtt.networking.model.Venues;
import giavu.hh.wtt.utils.Utils;

/**
 * @Author: Hoang Vu
 * @Date: 2018/06/22
 */

public class DriverPresenter implements
        DriverContract.Presenter
        ,DriverContract.TaxiCallback.onFinishListener{

    private final String TAG = DriverPresenter.class.getSimpleName();
    private DriverContract.TaxiCallback callback;

    private DriverContract.Model mModel;
    private DriverContract.View mView;
    private Context mContext;

    public DriverPresenter(DriverContract.View view, DriverContract.TaxiCallback callback, Context context){
        this.mView = view;
        this.mContext = context;
        this.callback = callback;
        mModel = new DriverModel();
        initPresenter();
    }

    private void initPresenter(){
        mView.initView();
    }
    @Override
    public void onFinished(List<Venues> data) {
        Log.d("TAG", "onFinished taxi screen");
        MarkerMap markerMap;
        List<MarkerMap> markerMapList = new ArrayList<MarkerMap>();
        for (Venues vo : data){
            markerMap = new MarkerMap();
            markerMap.setAddress(vo.getAddress1());
            markerMap.setLicense(Utils.getRandomDriverLicense(mContext));
            markerMap.setName(Utils.getRandomDriverName(mContext));
            markerMap.setRating((double)Utils.getRandom(5,1));
            markerMap.setRatingCount(Utils.getRandom(20,4));
            markerMap.setPhoneNumber(Utils.getRandomPhone(mContext));
            markerMapList.add(markerMap);
        }
        if(mView != null){
            mView.fillRecycleView(markerMapList);
            mView.hideProgress();
        }

    }

    @Override
    public void onFailure(Throwable throwable) {
        if(mView != null){
            mView.hideProgress();
        }
    }

    @Override
    public void onDestroy() {
        mView = null;
    }

    @Override
    public void fetchDataTaxi(Point point) {
        if(mView != null){
            mView.showProgress();
        }
        callback.getDriverList(point, this);
    }
}
