package giavu.hh.wtt.presenter;

import android.view.View;

import giavu.hh.wtt.contract.MainContract;
import giavu.hh.wtt.model.MainModel;

/**
 * @Author: Hoang Vu
 * @Date: 6/8/2018
 */

public class MainPresenter implements MainContract.Presenter{
    private final String TAG = MainPresenter.class.getSimpleName();
    private MainContract.View mView;
    private MainContract.Model mModel;


    public MainPresenter(MainContract.View view){
        this.mView = view;
        initPresenter();
    }

    private void initPresenter(){
        mView.initView();
        mModel = new MainModel();

    }

    @Override
    public void onClickHOIMAP(View view) {
        mView.navigateToMap();
    }
}
