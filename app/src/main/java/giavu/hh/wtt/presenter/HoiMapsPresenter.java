package giavu.hh.wtt.presenter;

import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.util.Log;
import android.view.View;

import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.MarkerOptions;

import java.util.ArrayList;
import java.util.List;

import giavu.hh.wtt.contract.HoiMapsContract;
import giavu.hh.wtt.model.HoiMapsModel;
import giavu.hh.wtt.networking.model.Driver;
import giavu.hh.wtt.networking.model.MarkerMap;
import giavu.hh.wtt.networking.model.Point;
import giavu.hh.wtt.networking.model.ResultDriverData;
import giavu.hh.wtt.networking.model.Venues;
import giavu.hh.wtt.utils.Constants;
import giavu.hh.wtt.utils.Utils;
import giavu.hh.wtt.view.DriverActivity;
import giavu.hh.wtt.view.HoiMapsActivity;

/**
 * @Author: Hoang Vu
 * @Date: 2018/06/20
 */

public class HoiMapsPresenter implements HoiMapsContract.Presenter
        , HoiMapsContract.DriverCallback.OnFinishListener
        , HoiMapsContract.DriverCallback.OnFinishListenerVenues {
    private final String TAG = HoiMapsPresenter.class.getSimpleName();
    private HoiMapsContract.View mView;
    private HoiMapsContract.Model mModel;

    private HoiMapsContract.DriverCallback callback;
    private Point currentPos;
    private Context ctx;

    public HoiMapsPresenter(HoiMapsContract.View view, HoiMapsContract.DriverCallback callback, Context ctx){
        this.mView = view;
        this.callback = callback;
        this.ctx = ctx;
        mModel = new HoiMapsModel();
        currentPos = new Point();
        initPresenter();
    }

    private void initPresenter(){
        mView.initView();
    }

    @Override
    public void clickMenu(View view, Point p) {
        Log.d(TAG, "Menu clicked !");
        Intent i = new Intent((HoiMapsActivity)ctx, DriverActivity.class);
        i.putExtra(Constants.LOCATION_LATITUDE_DATA_EXTRA, p.getLat());
        i.putExtra(Constants.LOCATION_LONGITUDE_DATA_EXTRA, p.getLon());
        ((HoiMapsActivity) ctx).startActivityForResult(i,Constants.MAP_CODE);
    }

    @Override
    public void onDestroy() {
        mView = null;
    }

    @Override
    public void findDriver() {
        if(mView != null){
            mView.showProgress();
        }
        callback.getDriverNearby(this);
    }

    @Override
    public void findVenues(Point p) {
        callback.getVenuesList(p, this);
    }

    @Override
    public void setCurrentPos(Point pos) {
        currentPos.setLat(pos.getLat());
        currentPos.setLon(pos.getLon());
    }

    @Override
    public void phoneCall(String number) {
        Intent intentCall = new Intent(Intent.ACTION_CALL);
        intentCall.setData(Uri.parse("tel:" + number));
        //ctx.startActivity(intentCall);
    }


    @Override
    public void onFinished(ResultDriverData resData) {
        List<Driver> list = resData.getDrives();
    }

    @Override
    public void onFinished(List<Venues> resData) {
        Log.d("TAG", "list");
        // create marker
        Point p;
        MarkerOptions markerOptions;
        MarkerMap markerMap;
        List<MarkerMap> markerMapList = new ArrayList<MarkerMap>();
        for (Venues vo : resData){
            markerMap = new MarkerMap();
            p = Utils.getLocation(currentPos.getLon(),currentPos.getLat() ,200);
            markerOptions = new MarkerOptions().position(new LatLng(p.getLat(), p.getLon()));
            markerMap.setMarkerOptions(markerOptions);
            markerMap.setAddress(vo.getAddress1());
            markerMap.setLicense(Utils.getRandomDriverLicense(ctx));
            markerMap.setName(Utils.getRandomDriverName(ctx));
            markerMap.setRating((double)Utils.getRandom(5,1));
            markerMap.setRatingCount(Utils.getRandom(20,4));
            markerMap.setPhoneNumber(Utils.getRandomPhone(ctx));
            markerMapList.add(markerMap);
        }
        mView.showMarkerNearCP(markerMapList);
    }

    @Override
    public void onFailure(Throwable t) {
        Log.d(TAG, t.getMessage());
    }

}
