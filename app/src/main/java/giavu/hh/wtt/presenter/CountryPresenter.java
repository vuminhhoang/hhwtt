package giavu.hh.wtt.presenter;

import giavu.hh.wtt.contract.CountryContract;
import giavu.hh.wtt.model.CountryModel;
import giavu.hh.wtt.networking.model.ResData;

/**
 * Created by hoangvm on 2018/06/11.
 */
public class CountryPresenter implements CountryContract.Presenter, CountryContract.CountryCallBack.OnFinishListener {
    private final String TAG = CountryPresenter.class.getSimpleName();
    private CountryContract.View mView;
    private CountryContract.Model mModel;

    private CountryContract.CountryCallBack callback;

    public CountryPresenter(CountryContract.View view, CountryContract.CountryCallBack callback){
        this.mView = view;
        this.callback = callback;
        mModel = new CountryModel();
        initPresenter();

    }

    private void initPresenter(){
        mView.initView();
    }

    @Override
    public void onFinished(ResData resData) {
        if (mView != null){
            mView.setDataToRecycleView(resData.getRestResponse().getResult());
            mView.hideProgress();
        }
    }

    @Override
    public void onFailure(Throwable t) {
        if (mView != null){
            mView.onResponseFailure(t);
            mView.hideProgress();
        }
    }

    @Override
    public void onDestroy() {
        mView = null;
    }

    @Override
    public void onRefreshButtonOnclick() {
        if(mView != null){
            mView.showProgress();
        }
        callback.getCountryList(this);
    }

    @Override
    public void requestDataFromServer() {
        if(mView != null){
            mView.showProgress();
        }
        callback.getCountryList(this);
    }
}
