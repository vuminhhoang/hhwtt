package giavu.hh.wtt.utils;

import android.content.Context;
import android.content.res.Resources;
import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.graphics.Matrix;
import android.graphics.Paint;

import java.util.Random;

import giavu.hh.wtt.R;
import giavu.hh.wtt.networking.model.Point;

/**
 * Created by hoangvm on 2018/06/20.
 */
public class Utils {
    public static int getStatusBarHeight(Context c) {
        int resourceId = c.getResources()
                .getIdentifier("status_bar_height", "dimen", "android");
        if (resourceId > 0) {
            return c.getResources().getDimensionPixelSize(resourceId);
        }
        return 0;
    }
    public static int pxToDp(int px)
    {
        return (int) (px / Resources.getSystem().getDisplayMetrics().density);
    }

    public static int dpToPx(int dp)
    {
        return (int) (dp * Resources.getSystem().getDisplayMetrics().density);
    }

    public static Point getLocation(double x0, double y0, int radius) {
        Random random = new Random();

        // Convert radius from meters to degrees
        double radiusInDegrees = radius / 111000f;

        double u = random.nextDouble();
        double v = random.nextDouble();
        double w = radiusInDegrees * Math.sqrt(u);
        double t = 2 * Math.PI * v;
        double x = w * Math.cos(t);
        double y = w * Math.sin(t);

        // Adjust the x-coordinate for the shrinking of the east-west distances
        double new_x = x / Math.cos(Math.toRadians(y0));

        double foundLongitude = new_x + x0;
        double foundLatitude = y + y0;
        Point newPoint = new Point();
        newPoint.setLon(foundLongitude);
        newPoint.setLat(foundLatitude);
        return newPoint;
    }

    public static Bitmap scaleBitmap(Bitmap bitmap, int newWidth, int newHeight) {
        Bitmap scaledBitmap = Bitmap.createBitmap(newWidth, newHeight, Bitmap.Config.ARGB_8888);

        float scaleX = newWidth / (float) bitmap.getWidth();
        float scaleY = newHeight / (float) bitmap.getHeight();
        float pivotX = 0;
        float pivotY = 0;

        Matrix scaleMatrix = new Matrix();
        scaleMatrix.setScale(scaleX, scaleY, pivotX, pivotY);

        Canvas canvas = new Canvas(scaledBitmap);
        canvas.setMatrix(scaleMatrix);
        canvas.drawBitmap(bitmap, 0, 0, new Paint(Paint.FILTER_BITMAP_FLAG));

        return scaledBitmap;
    }

    /**
     * Method return random number between min and max
     * @param max
     * @param min
     * @return random number
     */
    public static int getRandom(int max, int min){
        Random rand = new Random();
        int number = rand.nextInt(max) + min;
        return number;
    }

    /**
     * Return random string of driver
     * @param context
     * @return
     */
    public static String getRandomDriverName(Context context){
        String[] arrayOfStrings = context.getResources().getStringArray(R.array.drivers_name);
        String randomString = arrayOfStrings[new Random().nextInt(arrayOfStrings.length)];
        return randomString;
    }

    /**
     * Return random string of driver
     * @param context
     * @return
     */
    public static String getRandomCompanyName(Context context){
        String[] arrayOfStrings = context.getResources().getStringArray(R.array.companyArray);
        String randomString = arrayOfStrings[new Random().nextInt(arrayOfStrings.length)];
        return randomString;
    }



    /**
     * Return random string of license
     * @param context
     * @return
     */
    public static String getRandomDriverLicense(Context context){
        String[] arrayOfStrings = context.getResources().getStringArray(R.array.drivers_license);
        String randomString = arrayOfStrings[new Random().nextInt(arrayOfStrings.length)];
        return randomString;
    }
    /**
     * Return random string of phone number
     * @param context
     * @return
     */
    public static String getRandomPhone(Context context){
        String[] arrayOfStrings = context.getResources().getStringArray(R.array.drivers_phone);
        String randomString = arrayOfStrings[new Random().nextInt(arrayOfStrings.length)];
        return randomString;
    }
}
