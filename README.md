# Project Name
- Show motobike(taxi) position to find and call    

## Description
- Fetching the latest data from API then parsing the data  
- Show motobike nearby current position  

Table of Contents
-----------------
* [App screen demo](#App)
* [Environment](#environment)
* [Installation](#installation)
* [Library](#library)
* [License](#license)


App
----
App main screen  
![Screen1](https://gitlab.com/vuminhhoang/hhwtt/raw/master/img1.png)  

![Screen2](https://gitlab.com/vuminhhoang/hhwtt/raw/master/img2.png)  

![Screen3](https://gitlab.com/vuminhhoang/hhwtt/raw/master/img3.png)  

![Screen4](https://gitlab.com/vuminhhoang/hhwtt/raw/master/img4.png)  

Environment
----
- Android Studio 3.0.1  
- minsdkversion 21  
- compileSdkVersion 27  
- Java 1.8  

Installation
----
- Clone source code  
``git clone https://gitlab.com/vuminhhoang/hhwtt.git``
- Start Android Studio -> Import project -> It takes too long for the first time indexing and building project.

Library
----
- rxJava, rxAndroid  
- gson  
- glide  
- retrofit  
- butter knife  
- event bus


License
----
No license
