Keystore name: debug.keystore
Keystore password: android
Key alias: androiddebugkey
Key password: android

#Command to get key fingerprints
keytool -list -v -keystore debug.keystore -alias androiddebugkey -storepass android -keypass android 

#SSH1
E4:20:5C:D0:CA:64:83:ED:43:E7:3F:31:1A:39:97:94:3A:8F:D0:B3

#command to generate KEY STORE
keytool -genkey -v -keystore debug.keystore -storepass android -alias androiddebugkey -keypass android -keyalg RSA -keysize 2048 -validity 100000

